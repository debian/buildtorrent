buildtorrent (0.8-8) unstable; urgency=medium

  * QA upload
  * Fix lintian warning and info

  [ Jelmer Vernooĳ ]
  * Migrate repository from alioth to salsa.

 -- Bastian Germann <bage@debian.org>  Sat, 16 Sep 2023 22:05:58 +0200

buildtorrent (0.8-7) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #920088)
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control: bumped Standards Version to 4.5.0.

 -- Natan Mourao <nata.36014@gmail.com>  Sat, 09 May 2020 15:03:30 -0300

buildtorrent (0.8-6) unstable; urgency=medium

  * debian/control
    - (Homepage): Update to new location.
  * debian/copyright
    - Update all upstream URLs. Add Git address
  * debian/watch
    - Update location

 -- Jari Aalto <jari.aalto@cante.net>  Sat, 29 Oct 2016 18:13:01 +0300

buildtorrent (0.8-5) unstable; urgency=medium

  * debian/control
    - (Homepage): Update to Sourceforge.
    - (Standards-Version): Update to 3.9.8.
    - (Vcs-*): Update to anonscm.debian.org.
  * debian/copyright
    - (X-*): remove obsolete gitorious.org.

 -- Jari Aalto <jari.aalto@cante.net>  Tue, 18 Oct 2016 18:21:47 +0300

buildtorrent (0.8-4) unstable; urgency=low

  * debian/control
    - (Build-Depends): Rm dpkg-dev; not needed with debhelper 9.
    - (standards-Version): Update to 3.9.3.1.
  * debian/copyright
    - Update to format 1.0.
  * debian/rules
     - Enable all hardening flags.
     - Use DEB_*_MAINT_* variables.

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 22 Mar 2012 13:20:18 -0400

buildtorrent (0.8-3) unstable; urgency=low

  * debian/compat
    - Update to 9
  * debian/control
    - (Build-Depends): Update to debhelper 9, dpkg-dev 1.16.1
    - (Standards-Version): Update to 3.9.2.
  * debian/copyright
    - Update to latest DEP5 format.
  * debian/rules
    - Remove unnecessary rules.
    - Use hardened CFLAGS (release goal).
      http://wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags

 -- Jari Aalto <jari.aalto@cante.net>  Sat, 11 Feb 2012 15:21:15 -0500

buildtorrent (0.8-2) unstable; urgency=low

  * NOT RELEASED
  * debian/watch
    - Activate. Upstream provides new URL.

 -- Jari Aalto <jari.aalto@cante.net>  Tue, 09 Nov 2010 06:56:55 +0200

buildtorrent (0.8-1) unstable; urgency=low

  * New upstream release (Closes: #589927).
  * debian/compat
    - Update to 8.
  * debian/control
    - (Build-Depends): update to debhelper 8.
    - (Homepage): Update URL.
    - (Standards-Version): Update to 3.9.1.
  * debian/copyright
    - (Download): update URL.
    - (X-Upstream-Vcs-Homepage): New field.
  * debian/patches
    - (0001): Fix manual page problem: 'cannot adjust line'.
  * debian/watch
    - New file.

 -- Jari Aalto <jari.aalto@cante.net>  Mon, 11 Oct 2010 11:35:57 +0300

buildtorrent (0.7.2-5) unstable; urgency=low

  * New maintainer (Closes: #543834).
    - Move to packaging format "3.0 (quilt)".
  * debian/clean
    - New file.
  * debian/control
    - (Build-Depends): update to debhelper 7.1.
    - (Standards-Version): update to 3.8.4
    - (Vcs-*): new fields.
  * debian/copyright
    - (debian/*): Use FSF URL.
  * debian/rules
    - (override_dh_auto_clean): remove, not needed.
  * debian/source/format
    - New file.
  * debian/watch
    - New file.

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 11 Mar 2010 18:31:54 +0200

buildtorrent (0.7.2-4) unstable; urgency=low

  * Updating to standards version 3.8.3.
  * Removing vcs fields.
  * Orphaning package.

 -- Daniel Baumann <daniel@debian.org>  Thu, 27 Aug 2009 07:04:36 +0200

buildtorrent (0.7.2-3) unstable; urgency=low

  * Sorting depends.
  * Minimizing rules file.

 -- Daniel Baumann <daniel@debian.org>  Mon, 03 Aug 2009 02:05:50 +0200

buildtorrent (0.7.2-2) unstable; urgency=low

  * Replacing obsolete dh_clean -k with dh_prep.
  * Updating year in copyright file.
  * Upgrading package to standards version 3.8.2.
  * Updating rules file to current state of the art.
  * Prefixing debhelper files with package name.

 -- Daniel Baumann <daniel@debian.org>  Fri, 17 Jul 2009 00:25:33 +0200

buildtorrent (0.7.2-1) unstable; urgency=low

  * Initial release.

 -- Daniel Baumann <daniel@debian.org>  Tue, 14 Oct 2008 10:21:00 +0200
